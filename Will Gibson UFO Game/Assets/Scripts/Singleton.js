﻿var objectTag : String;
var objectsOfTag : GameObject[];
var startLevel;

var destroyOther : boolean = true;

function Start()
	{
	DontDestroyOnLoad(gameObject);
	objectTag = gameObject.tag;
	objectsOfTag = GameObject.FindGameObjectsWithTag(objectTag);
	
	if(objectsOfTag.Length >=2)
		{
		if(destroyOther == true)
			{
			Destroy(objectsOfTag[1]);
			}
		else
			{
			Destroy(gameObject);
			}
		}
	}