﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{

    public GameObject player;

    void Start()
    {
    }

    void LateUpdate()
    {
        transform.position = player.transform.position;
    }
}