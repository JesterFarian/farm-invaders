﻿using UnityEngine;
using System.Collections;

public class FuelMeter : MonoBehaviour {

	public float progress = 0f;
	public float usage = 0.13f;
	public float recharge = 0.18f;
	public float current;
	public Vector2 pos = new Vector2(0.67f, 0.02f);
	public Vector2 size = new Vector2(256,256);
	public Texture2D emptyTex;
	public Texture2D fullTex;
	public SpriteRenderer[] spriterenderer;


	void OnGUI() {

		float posX = Screen.width * pos.x;
		float posY = Screen.height * pos.y;

		GUI.BeginGroup(new Rect(posX, posY, size.x, size.y));
		GUI.DrawTexture(new Rect(0,0, size.x, size.y), emptyTex);

		int xProg = (int)(size.x * progress);
		GUI.BeginGroup(new Rect(size.x - xProg, 0, xProg, size.y));
		GUI.DrawTexture(new Rect(-size.x + xProg,0, size.x, size.y), fullTex);

		GUI.EndGroup();
		GUI.EndGroup();
	}
		
	void Start () {
		current = progress;

	}
	void Update() {
			progress = Mathf.Clamp (progress, 0f, 1f);
		current = progress;
		if (Input.GetMouseButton (0))
			progress = current + usage * Time.deltaTime;
		else
			progress = current - recharge * Time.deltaTime;

        if (Input.GetMouseButton(0))
        {
            GameObject.Find("Beam").GetComponent<SpriteRenderer>().enabled = true;
        }
        if (Input.GetMouseButton(0))
        {
            GameObject.Find("Beam").GetComponent<BoxCollider2D>().enabled = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            GameObject.Find("Beam").GetComponent<SpriteRenderer>().enabled = false;
        }
        if (progress >= 1f)
        {
            GameObject.Find("Beam").GetComponent<SpriteRenderer>().enabled = false;
        }
        if (Input.GetMouseButtonUp(0))
        {
            GameObject.Find("Beam").GetComponent<BoxCollider2D>().enabled = false;
        }
        if (progress >= 1f)
            {
            GameObject.Find("Beam").GetComponent<BoxCollider2D>().enabled = false;
            }
	}
}