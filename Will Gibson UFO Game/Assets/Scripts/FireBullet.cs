﻿using UnityEngine;
using System.Collections;  
 
public class FireBullet : MonoBehaviour
{
    public GameObject shotPrefab;
    public float speed = 500f;
    public int clipSize = 8;
    public int remaining;
    public float shootDelay = 2f;
    private float shootTimer;

    void start()
    {
        remaining = clipSize;
    }

    void Update()
    {
        if (remaining > 0)
        {
            if (Input.GetMouseButton(1))
            {
                if (shootTimer <= 0)
                {
                    GameObject Bullet = Instantiate(shotPrefab, transform.position, transform.rotation);
                    Bullet.GetComponent<Rigidbody2D>().velocity = transform.up * speed;
                    remaining = remaining - 1;
                    shootTimer = shootDelay;
                }       
            }
        }
        shootTimer = shootTimer - 1 * Time.deltaTime;
        if (Input.GetKeyDown("r"))
        {
            remaining = clipSize;
        }
    }
}
