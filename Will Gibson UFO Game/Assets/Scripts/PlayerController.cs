﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;
    public Text overText;
    public Text reloadText;
    public Text ammoText;
    public PlayerHealth PH;
    public float pickUpTimer;
    public float pickUpDelay = 2f;
    public bool pickUpCow = false;
    public GameObject smooshPrefab;
    public FireBullet FB;

    private Rigidbody2D rb2d;
    private int count;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        count = 0;
        countText.text = "Score: " + count.ToString();
        winText.text = "";
        overText.text = "";
        reloadText.text = "";
        ammoText.text = "0/8";
        GameObject Player = GameObject.Find("Player");
        PlayerHealth PH = Player.GetComponent<PlayerHealth>();
        pickUpTimer = pickUpDelay;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        rb2d.AddForce(movement * speed);
        SetCountText();
    }

    void SetCountText()
    {
        ammoText.text = (FB.remaining + "/8");
        if (count >= 10)
            winText.text = "You Win!";
        if (PH.progress >= 1)
            overText.text = "Game Over!";
        if (FB.remaining == 0)
            reloadText.text = "Reload";
        if (FB.remaining > 0)
            reloadText.text = "";
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (Input.GetKeyDown("space"))
        {
            pickUpCow = true;
            pickUpTimer = pickUpDelay;
        }
        if (pickUpCow == true)
        {
            pickUpTimer = pickUpTimer - 1 * Time.deltaTime;
        }

        if (Input.GetKeyUp("space"))
        {
            if (pickUpTimer > 0)
            {
                Debug.Log("Cow Smoosh");
                pickUpCow = false;
                other.gameObject.SetActive(false);
                GameObject Smoosh = Instantiate(smooshPrefab, transform.position, transform.rotation);
            }
            if (pickUpTimer < 0)
            {
                Debug.Log("Cow Safe");
                pickUpCow = false;
                other.gameObject.SetActive(false);
                count = count + 1;
                countText.text = "Score: " +count.ToString();
            }
            pickUpTimer = pickUpDelay;
        }
    }
}