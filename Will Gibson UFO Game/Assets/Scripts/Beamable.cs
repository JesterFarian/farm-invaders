﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beamable : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D coll)
    {
        Debug.Log("Colision!");
        if (coll.gameObject.tag == "Beam")
        {
            Destroy(gameObject);
        }
    }
}